package com.klungerbo.bonusapp.model.member;

import com.klungerbo.bonusapp.model.Personals;

import java.time.LocalDate;

/**
 * Representation for silver membership
 */
public class SilverMember extends BonusMember {
    public static final float FACTOR_SILVER = 1.2f;

    public SilverMember(int memberNo, Personals personals, LocalDate localDate, int points) {
        super(memberNo, personals, localDate);
        this.addPoints(points);
    }

    /**
     * Apply bonus points and register it for the bonus member
     *
     * @param points the points add
     */
    @Override
    public void registerPoints(int points) {
        points *= FACTOR_SILVER;
        this.addPoints(points);
    }
}
