package com.klungerbo.bonusapp.model.member;

import com.klungerbo.bonusapp.model.Personals;

import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;

/**
 * Data class for a bonus member
 */
public abstract class BonusMember implements Comparable<BonusMember> {
    private final int memberNo;
    private final Personals personals;
    private final LocalDate enrolledDate;
    private int bonusPoints;

    protected BonusMember(int memberNo, Personals personals, LocalDate enrolledDate) {
        this.bonusPoints = 0;
        this.memberNo = memberNo;
        this.personals = personals;
        this.enrolledDate = enrolledDate;
    }

    public int getMemberNo() {
        return this.memberNo;
    }

    public Personals getPersonals() {
        return this.personals;
    }

    public LocalDate getEnrolledDate() {
        return this.enrolledDate;
    }

    public int getPoints() {
        return this.bonusPoints;
    }

    /**
     * Finds the qualification points i.e. points that are register
     * from enrolled date up to maximum a year. If a year has passed
     * since enrollment there will be no qualification points.
     *
     * @param currentDate the current date
     * @return points if less than a year of membership, 0 if else.
     */
    public int findQualificationPoints(LocalDate currentDate) {
        int returnValue = 0;

        if (currentDate != null) {
            int yearsBetween = Period.between(this.enrolledDate, currentDate).getYears();
            if (yearsBetween < 1) {
                returnValue = this.getPoints();
            }
        }

        return returnValue;
    }

    /**
     * Delegates password checking to the personals
     *
     * @param password the password to check
     * @return true if password is correct, false if else
     */
    public boolean okPassword(String password) {
        boolean returnValue = false;

        if (password != null) {
            returnValue = this.personals.okPassword(password);
        }

        return returnValue;
    }

    /**
     * Register points to the member
     *
     * @param points the points to add
     */
    protected void addPoints(final int points) {
        if (points > 0) {
            this.bonusPoints += points;
        }
    }

    public abstract void registerPoints(final int points);


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BonusMember that = (BonusMember) o;
        return memberNo == that.memberNo &&
                bonusPoints == that.bonusPoints &&
                Objects.equals(personals, that.personals) &&
                Objects.equals(enrolledDate, that.enrolledDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(memberNo, personals, enrolledDate, bonusPoints);
    }

    @Override
    public int compareTo(BonusMember bonusMember) {
        int returnVal;

        if (this.bonusPoints < bonusMember.bonusPoints) {
            returnVal = -1;
        }
        else if (this.bonusPoints == bonusMember.bonusPoints) {
            returnVal = 0;
        }
        else {
            returnVal = 1;
        }

        return returnVal;
    }
}
