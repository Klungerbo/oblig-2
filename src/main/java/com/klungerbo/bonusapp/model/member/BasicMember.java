package com.klungerbo.bonusapp.model.member;

import com.klungerbo.bonusapp.model.Personals;

import java.time.LocalDate;

/**
 * Representation for basic membership
 */
public class BasicMember extends BonusMember {
    public BasicMember(int memberNo, Personals personals, LocalDate localDate) {
        super(memberNo, personals, localDate);
    }

    @Override
    public void registerPoints(int points) {
        this.addPoints(points);
    }
}
