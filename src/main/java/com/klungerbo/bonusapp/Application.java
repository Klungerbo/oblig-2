/** Bonus member application. **/
package com.klungerbo.bonusapp;

import com.klungerbo.bonusapp.ui.MemberUi;

public final class Application {
    private Application() {
    }

    /**
     * Entry point for the application.
     *
     * @param args command line arguments
     */
    public static void main(final String[] args) {
        MemberUi ui = new MemberUi();
        ui.init();
        ui.execute();
    }
}
