package com.klungerbo.bonusapp.logic;

import com.klungerbo.bonusapp.model.Personals;
import com.klungerbo.bonusapp.model.member.BasicMember;
import com.klungerbo.bonusapp.model.member.BonusMember;
import com.klungerbo.bonusapp.model.member.GoldMember;
import com.klungerbo.bonusapp.model.member.SilverMember;

import java.time.LocalDate;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;
import java.util.Random;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Manages bonus members
 */
public class MemberArchive implements Iterable<BonusMember>{
    private static final int SILVER_LIMIT = 25000;
    private static final int GOLD_LIMIT = 75000;

    private List<BonusMember> members;

    public MemberArchive() {
        this.members = new ArrayList<>();
    }

    /**
     * Upgrade a bonus member to a silver member if allowed
     *
     * @param member the member to upgrade
     * @param date the current date
     * @return the member in its current form
     */
    private BonusMember upgradeToSilver(BonusMember member, LocalDate date) {
        BonusMember returnMember = member;

        if (this.canUpgradeToSilver(member, date)) {
            returnMember = new SilverMember(
                    member.getMemberNo(),
                    member.getPersonals(),
                    member.getEnrolledDate(),
                    member.getPoints()
            );
        }

        return returnMember;
    }

    /**
     * Upgrade a bonus member to a silver member if allowed
     *
     * @param member the member to upgrade
     * @param date the current date
     * @return the member in its current form
     */
    private BonusMember upgradeToGold(BonusMember member, LocalDate date) {
        BonusMember returnMember = member;

        if (this.canUpgradeToGold(member, date)) {
            returnMember = new GoldMember(
                    member.getMemberNo(),
                    member.getPersonals(),
                    member.getEnrolledDate(),
                    member.getPoints()
            );
        }

        return returnMember;
    }

    /**
     * Checks if a member can be upgraded to silver
     *
     * @param member the member that wants to be upgraded
     * @param date the current date
     * @return true if it can be upgraded to silver, false if else
     */
    private boolean canUpgradeToSilver(BonusMember member, LocalDate date) {
        int collectedPoints = member.findQualificationPoints(date);

        boolean validForUpgrade = member instanceof BasicMember;
        validForUpgrade = validForUpgrade && collectedPoints >= SILVER_LIMIT;

        return validForUpgrade;
    }

    /**
     * Checks if a member can be upgraded to gold
     *
     * @param member the member that wants to be upgraded
     * @param date the current date
     * @return true if the member can be upgraded to gold, false if else
     */
    private boolean canUpgradeToGold(BonusMember member, LocalDate date) {
        int collectedPoints = member.findQualificationPoints(date);

        boolean validForUpgrade = member instanceof BasicMember || member instanceof SilverMember;
        validForUpgrade = validForUpgrade && collectedPoints > 0;
        validForUpgrade = validForUpgrade && collectedPoints >= GOLD_LIMIT;

        return validForUpgrade;
    }

    /**
     * Uses streams to upgrade bonus members that are qualified
     *
     * NOTE: Should not be used as it creates a new array each
     * time upgrades happens
     *
     * @param date the current date
     */
    public void checkMembersWithStreams(LocalDate date) {
        this.members = this.members
                .stream()
                .map(member -> {
                    BonusMember returnMember = member;

                    if (this.canUpgradeToGold(member, date)) {
                        returnMember = this.upgradeToGold(member, date);
                    } else if (this.canUpgradeToSilver(member, date)) {
                        returnMember = this.upgradeToSilver(member, date);
                    }

                    return returnMember;
                })
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Checks through all members and upgrades those who are
     * qualified
     *
     * @param date the current date
     */
    public void checkMembers(LocalDate date) {
        for (int i = 0; i < this.members.size(); i++) {
            BonusMember member = this.members.get(i);

            if (this.canUpgradeToGold(member, date)) {
                BonusMember goldMember = this.upgradeToGold(member, date);
                this.members.set(i, goldMember);
            } else if (this.canUpgradeToSilver(member, date)) {
                BonusMember silverMember = this.upgradeToSilver(member, date);
                this.members.set(i, silverMember);
            }
        }
    }

    /**
     * Add a bonus member to the archive
     *
     * @param pers the personals information connected with the membership
     * @param dateEnrolled the date this member enrolled as a member
     * @return the members identification number
     */
    public int addMember(Personals pers, LocalDate dateEnrolled) {
        int memberNo = this.findAvailableNo();
        BonusMember member = new BasicMember(memberNo, pers, dateEnrolled);
        this.members.add(member);

        return memberNo;
    }

    /**
     * Find an available member number
     * @return the available member number
     */
    private int findAvailableNo() {
        Random rand = new Random();
        int memberNo = -1;

        while (memberNo == -1) {
            int num = rand.nextInt(Integer.MAX_VALUE);
            if (this.getMember(num) == null) {
                memberNo = num;
            }
        }

        return memberNo;
    }

    /**
     * Register points to a member
     *
     * @param memberNo the member identification number to add points to
     * @param points the points to be added
     * @return true if the member exists and it received its points, false if else
     */
    public boolean registerPoints(int memberNo, int points) {
        BonusMember member = this.getMember(memberNo);
        if (member != null) {
            member.registerPoints(points);
        }

        return member != null;
    }


    /**
     * Finds the points connected to a specific member
     *
     * @param memberNo the member identification number to look for
     * @param password the password of the member for verification
     * @return the ammount of points the member has
     */
    public int findPoints(int memberNo, String password) {
        int returnValue = -1;

        BonusMember member = this.getMember(memberNo);
        if (member != null) {
            if (member.okPassword(password)) {
                returnValue = member.getPoints();
            }
        }

        return returnValue;
    }

    /**
     * Finds a member by its identification number
     *
     * @param memberNo the member identification number
     * @return the member if it was found, null if else
     */
    public BonusMember getMember(int memberNo) {
        Optional<BonusMember> member = this.members
                .parallelStream()
                .filter(bonusMember -> bonusMember.getMemberNo() == memberNo)
                .findFirst();

        return member.orElse(null);
    }

    public List<BonusMember> getArchive() {
        return this.members;
    }

    @Override
    public Iterator<BonusMember> iterator() {
        return this.members.iterator();
    }

    @Override
    public void forEach(Consumer<? super BonusMember> action) {
        this.members.forEach(action);
    }
}
