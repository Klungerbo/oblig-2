package com.klungerbo.bonusapp;

import com.klungerbo.bonusapp.logic.MemberArchive;
import com.klungerbo.bonusapp.model.member.BasicMember;
import com.klungerbo.bonusapp.model.member.BonusMember;
import com.klungerbo.bonusapp.model.member.GoldMember;
import com.klungerbo.bonusapp.model.member.SilverMember;
import com.klungerbo.bonusapp.model.Personals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit test class for testing member archive functionality
 */
class MemberArchiveTest {
    private LocalDate testDate;
    private Personals ole;
    private Personals tove;

    private MemberArchive memberArchive;

    /**
     * Set up defaults for fields
     */
    @BeforeEach
    void setUp() {
        this.memberArchive = new MemberArchive();
        this.testDate = LocalDate.of(2008, 2, 10);
        this.ole = new Personals("Olsen", "Ole", "ole.olsen@dot.com", "ole");
        this.tove = new Personals("Hansen", "Tove", "tove.hansen@dot.com", "tove");
    }

    /**
     * Checks if bonus membership upgrading is behaving as expected.
     */
    @Test
    void testCheckMembersMultipleUpgrades() {
        int memberNo1 = this.memberArchive.addMember(ole, LocalDate.of(2008, 1, 11));
        this.memberArchive.registerPoints(memberNo1, 100000);

        int memberNo2 = this.memberArchive.addMember(tove, LocalDate.of(2008, 1, 20));
        this.memberArchive.registerPoints(memberNo2, 100);

        this.memberArchive.checkMembers(this.testDate);

        BonusMember mem1 = this.memberArchive.getMember(memberNo1);
        assertNotNull(mem1);
        assertTrue(mem1 instanceof GoldMember);

        BonusMember mem2 = this.memberArchive.getMember(memberNo2);
        assertNotNull(mem2);
        assertTrue(mem2 instanceof BasicMember);

        this.memberArchive.registerPoints(memberNo2, 30000);

        this.memberArchive.checkMembers(this.testDate);

        mem1 = this.memberArchive.getMember(memberNo1);
        assertNotNull(mem1);
        assertTrue(mem1 instanceof GoldMember);

        mem2 = this.memberArchive.getMember(memberNo2);
        assertNotNull(mem2);
        assertTrue(mem2 instanceof SilverMember);
    }
}